<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	
	<form action="/welcome" method="post">
		@csrf
		<label for="firstName">First Name:</label>
		<input type="text" name="firstName"> <br>
		<label for="lastName">Last Name:</label><br>
		<input type="text" name="lastName">

		<p>Gender :</p>
		<input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label><br>		

		<p>Nationality</p>
		<select>
			<option>Indonesia</option>
			<option>Malaysia</option>
			<option>Singapore</option>
			<option>Thailand</option>
		</select>
		<p>Language :</p>
		<input type="checkbox" name="bhs">Indonesian <br>
		<input type="checkbox" name="bhs">English <br>
		<input type="checkbox" name="bhs">Other <br>
		<p>Bio</p>
		<textarea name="bio" cols="30" rows="10"></textarea>
		<input type="submit" value="Sign Up">

	</form>
</body>
</html>